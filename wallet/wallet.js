const ChainUtil = require('./chain_util');
const Transaction = require('./transaction');
const {INITIAL_BALANCE} = require('../config');

class Wallet {
  constructor(secret) {
    this.balance = INITIAL_BALANCE;
    this.keyPair = ChainUtil.genKeyPair(secret);
    this.publicKey = this.keyPair.getPublic("hex");
  }

  toString() {
    return `Wallet - 
        publicKey: ${this.publicKey.toString()}
        balance  : ${this.balance}`;
  }

  sign(dataHash) {
    return this.keyPair.sign(dataHash);
  }

  createTransaction(to, amount, type, blockchain, transactionPool) {
    let transaction = Transaction.newTransaction(this, to, amount, type);
    transactionPool.addTransaction(transaction);
    return transaction;
  }
}

module.exports = Wallet;
