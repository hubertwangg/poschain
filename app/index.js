const Blockchain = require('../blockchain/blockchain');
const bodyParser = require('body-parser');
const express = require('express');
const P2pServer = require('./p2pserver');

const HTTP_PORT = process.env.HTTP_PORT || 1212;

const app = express();
app.use(bodyParser.json());
const blockchain = new Blockchain();
const p2pServer = new P2pServer(blockchain);

// Wallet
const Wallet = require('../wallet/wallet');
const TransactionPool = require('../wallet/transaction_pool');
const transactionPool = new TransactionPool();
const wallet = new Wallet();

// Endpoints
app.get('/blocks', (req, res) => {
  res.json(blockchain.chain);
});

app.post('/mine', (req, res) => {
  const data = req.body.data;
  const block = blockchain.addBlock(data);
  console.log(`New block added: ${block.toString()}`);

  p2pServer.syncChain();
  res.redirect('/blocks');
});

app.get('/transactions', (req, res) => {
  res.json(transactionPool.transactions);
});

app.post('/transact', (req, res) => {
  const { to, amount, type } = req.body;
  const transaction = wallet.createTransaction(
    to, amount, type, blockchain, transactionPool
  );
  res.redirect('/transactions');
});

// Main function
app.listen(HTTP_PORT, () => {
  console.log('System listening on port:', HTTP_PORT);
});
p2pServer.listen();
